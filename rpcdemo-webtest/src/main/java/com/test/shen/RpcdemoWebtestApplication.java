package com.test.shen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.test.shen", "com.test.yu"})
public class RpcdemoWebtestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RpcdemoWebtestApplication.class, args);
    }
}
