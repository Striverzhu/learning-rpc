package com.test.shen.controller;

import com.test.shen.agent.ServiceAgent;
import com.test.yu.proxy.RpcProxy;
import com.test.zhu.model.Person;
import com.test.zhu.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xingyuzhu on 2018/3/21.
 */
@RestController
public class TestContrllor {
    @Autowired
    private ServiceAgent serviceAgent;

    @RequestMapping(value = "index")
    public String test() {
        String request = "zhuxingyu";
        Person response = serviceAgent.callMethod("hello", request, Person.class);
        return response.getName() + ":" + response.getAge();
    }
}
