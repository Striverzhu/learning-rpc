package com.test.shen.agent;

import com.test.yu.proxy.RpcProxy;
import com.test.zhu.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;

@Component
public class ServiceAgent {
    @Autowired
    private RpcProxy proxy;

    private HashMap<String, Class> classMap;

    public ServiceAgent() {
        classMap = new HashMap<>();
        classMap.put("hello", HelloService.class);
    }

    public <K, V> V callMethod(String method, K request, Class<V> cls) {
        Object object = proxy.create(classMap.get(method));
        Method[] methods = object.getClass().getDeclaredMethods();
        for (Method thod : methods) {
            if (method.equals(thod.getName())) {
                try {
                    Object response = thod.invoke(object, request);
                    return (V) response;
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
        return null;
    }
}
