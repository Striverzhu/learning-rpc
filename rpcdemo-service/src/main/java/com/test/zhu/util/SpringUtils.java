package com.test.zhu.util;

import org.springframework.context.ApplicationContext;

public class SpringUtils {
    private static ApplicationContext applicationContext;

    public static void setApplicationContext(ApplicationContext context) {
        applicationContext = context;
    }

    public static <T> T getBean(Class<T> beanName) {
        return applicationContext.getBean(beanName);
    }
}
