package com.test.zhu.service;

import com.test.zhu.aspectj.TraceDebug;
import com.test.zhu.model.Person;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
public interface HelloService {
    Person hello(String name);
}
