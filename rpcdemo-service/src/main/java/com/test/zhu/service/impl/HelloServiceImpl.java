package com.test.zhu.service.impl;

import com.test.xing.annotate.RpcService;
import com.test.zhu.aspectj.TraceDebug;
import com.test.zhu.model.Person;
import com.test.zhu.othermethod.OtherTest;
import com.test.zhu.repository.PersonWay;
import com.test.zhu.service.HelloService;
import com.test.zhu.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
@RpcService(HelloService.class)
public class HelloServiceImpl implements HelloService {

    @Autowired
    private PersonWay personWay;

    @Override
    public Person hello(String name) {
        System.out.println("wel....");
        return personWay.getPerson(name);
    }
}
