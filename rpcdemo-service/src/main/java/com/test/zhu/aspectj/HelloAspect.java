package com.test.zhu.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;

@Aspect
@Component
public class HelloAspect {
    @Pointcut("execution(@TraceDebug * *(..))")
    public void pointcut() {
    }

    @Before("pointcut()")
    public void debug(JoinPoint joinPoint) {
        System.out.println("wahahahah.....");
        try {
            FileOutputStream outputStream = new FileOutputStream("d:\\1.txt");
            outputStream.write("welcome".getBytes());
            outputStream.close();
        }catch (IOException ex){

        }
    }
}
