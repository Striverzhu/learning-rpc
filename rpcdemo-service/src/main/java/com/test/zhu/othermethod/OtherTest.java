package com.test.zhu.othermethod;

import com.test.zhu.service.HelloService;
import com.test.zhu.util.SpringUtils;

public class OtherTest {

    private HelloService helloService;

    public OtherTest() {
        helloService = SpringUtils.getBean(HelloService.class);
    }

    public String getAge() {
        return "29";
    }
}
