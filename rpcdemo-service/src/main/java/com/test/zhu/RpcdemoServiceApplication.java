package com.test.zhu;

import com.test.zhu.util.SpringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.test")
public class RpcdemoServiceApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RpcdemoServiceApplication.class, args);
        SpringUtils.setApplicationContext(context);
    }
}
