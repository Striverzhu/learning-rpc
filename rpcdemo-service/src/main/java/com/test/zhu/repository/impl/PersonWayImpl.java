package com.test.zhu.repository.impl;

import com.test.zhu.aspectj.TraceDebug;
import com.test.zhu.model.Person;
import com.test.zhu.repository.PersonWay;
import org.springframework.stereotype.Component;

@Component
public class PersonWayImpl implements PersonWay {

    @TraceDebug
    public Person getPerson(String name) {
        Person person = new Person();
        person.setName(name);
        person.setAge("28");
        person.setTall(165);
        return person;
    }
}
