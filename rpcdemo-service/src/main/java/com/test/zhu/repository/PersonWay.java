package com.test.zhu.repository;

import com.test.zhu.model.Person;

public interface PersonWay {
    Person getPerson(String name);
}
