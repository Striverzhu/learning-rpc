package com.test.xing.zkutil;

import com.test.xing.constance.Constant;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
@Component
public class ServiceRegistry {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRegistry.class);
    private CountDownLatch latch = new CountDownLatch(1);

    public void register(String data, String registryAddress) {
        if (data != null) {
            ZooKeeper zk = connectZookeeper(registryAddress);
            if (zk != null) {
                createNode(zk, data);
            }
        }
    }

    private ZooKeeper connectZookeeper(String registryAddress) {
        ZooKeeper zk = null;
        try {
            zk = new ZooKeeper(registryAddress, Constant.ZK_SESSION_TIMEOUT, watchedEvent -> {
                if (watchedEvent.getState() == Watcher.Event.KeeperState.SyncConnected) {
                    latch.countDown();
                }
            });
            latch.await();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return zk;
    }

    private void createNode(ZooKeeper zk, String data) {
        try {
            byte[] bytes = data.getBytes();
            String path = zk.create(Constant.ZK_DATA_PATH, bytes, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
            LOGGER.debug("create zk node ({}=>{})", path, data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }
}
