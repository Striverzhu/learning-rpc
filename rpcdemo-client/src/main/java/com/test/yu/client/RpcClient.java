package com.test.yu.client;

import com.test.serialize.rpcutil.RpcDecoder;
import com.test.serialize.rpcutil.RpcEncoder;
import com.test.serialize.rpcutil.RpcRequest;
import com.test.serialize.rpcutil.RpcResponse;
import com.test.yu.datacontainer.DataContainer;
import com.test.yu.handler.RpcHandler;
import com.test.yu.lock.Lock;
import com.test.yu.zktutil.ServiceDiscovery;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
@Component
@ChannelHandler.Sharable
public class RpcClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(RpcClient.class);
    @Autowired
    private ServiceDiscovery serviceDiscovery;

    public RpcResponse send(RpcRequest request) throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        public void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline()
                                    .addLast(new RpcEncoder(RpcRequest.class))
                                    .addLast(new RpcDecoder(RpcResponse.class))
                                    .addLast(new RpcHandler());
                        }
                    }).option(ChannelOption.SO_KEEPALIVE, true);
            String serviceAddress = serviceDiscovery.discover();
            String[] arrays = serviceAddress.split(":");
            ChannelFuture future = bootstrap.connect(arrays[0], Integer.parseInt(arrays[1])).sync();
            future.channel().writeAndFlush(request).sync();
            synchronized (Lock.lock) {
                Lock.lock.wait();
            }
            if (DataContainer.dataMap.get(request.getRequestId()) != null) {
                future.channel().closeFuture().sync();
            }
            return DataContainer.dataMap.get(request.getRequestId());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        } finally {
            group.shutdownGracefully();
        }
        return null;
    }
}
