package com.test.yu.zktutil;

import com.test.yu.constance.Constant;
import org.apache.logging.log4j.util.Strings;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
@Component
public class ServiceDiscovery {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceDiscovery.class);
    private CountDownLatch latch = new CountDownLatch(1);
    private volatile List<String> dataList = new ArrayList<>();
    @Value("${registry.address}")
    private String registryAddress;

    public String discover() {
        String data = getServiceAddress();
        if (Strings.isNotBlank(data)) {
            return data;
        } else {
            ZooKeeper zk = connectServer();
            if (zk != null) {
                watchNode(zk);
            }
            data = getServiceAddress();
        }
        return data;
    }

    private String getServiceAddress() {
        String data = null;
        int size = dataList.size();
        if (size > 0) {
            if (size == 1) {
                data = dataList.get(0);
                LOGGER.debug("using only data: {}", data);
            } else {
                data = dataList.get(ThreadLocalRandom.current().nextInt(size));
                LOGGER.debug("using random data: {}", data);
            }
        }
        return data;
    }

    private ZooKeeper connectServer() {
        ZooKeeper zk = null;
        try {
            zk = new ZooKeeper(registryAddress, Constant.ZK_SESSION_TIMEOUT, watchedEvent -> {
                if (watchedEvent.getState() == Watcher.Event.KeeperState.SyncConnected) {
                    latch.countDown();
                }
            });
            latch.await();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return zk;
    }

    private void watchNode(final ZooKeeper zk) {
        try {
            List<String> nodeList = zk.getChildren(Constant.ZK_REGISTRY_PATH, watchedEvent -> {
                if (watchedEvent.getType() == Watcher.Event.EventType.NodeChildrenChanged) {
                    watchNode(zk);
                }
            });
            List<String> dataList = new ArrayList<>();
            for (String it : nodeList) {
                byte[] bytes = zk.getData(Constant.ZK_REGISTRY_PATH + "/" + it, false, null);
                dataList.add(new String(bytes));
            }
            LOGGER.debug("node data:{}", dataList);
            this.dataList = dataList;
        } catch (KeeperException | InterruptedException ex) {
            LOGGER.error(ex.getMessage());
        }
    }
}
