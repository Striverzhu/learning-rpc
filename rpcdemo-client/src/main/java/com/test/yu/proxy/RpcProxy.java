package com.test.yu.proxy;

import com.test.serialize.rpcutil.RpcRequest;
import com.test.serialize.rpcutil.RpcResponse;
import com.test.yu.client.RpcClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * Created by xingyuzhu on 2018/3/20.
 */
@Component
public class RpcProxy {
    @Autowired
    private RpcClient rpcClient;

    public <T> T create(Class<?> interfaceClass) {
        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass},
                (proxy, method, args) -> {
                    RpcRequest request = new RpcRequest();
                    request.setRequestId(UUID.randomUUID().toString());
                    request.setClassName(method.getDeclaringClass().getName());
                    request.setMethodName(method.getName());
                    request.setParameterTypes(method.getParameterTypes());
                    request.setParameter(args);
                    RpcResponse response = rpcClient.send(request);
                    if (response.getError() != null) {
                        throw response.getError();
                    } else {
                        return response.getResult();
                    }
                });
    }
}
