package com.test.yu.handler;

import com.test.serialize.rpcutil.RpcResponse;
import com.test.yu.datacontainer.DataContainer;
import com.test.yu.lock.Lock;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class RpcHandler extends SimpleChannelInboundHandler<RpcResponse> {

    public void channelRead0(ChannelHandlerContext ctx, RpcResponse response) throws Exception {
        DataContainer.dataMap.put(response.getRequestId(), response);
        synchronized (Lock.lock) {
            Lock.lock.notifyAll();
        }
    }
}
