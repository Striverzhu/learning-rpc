zk安装：
1.下载zk安装包，解压到指定目录，将conf目录下的zoo-sample.cfg改名为zoo.cfg，并简单修改下该文件：
dataDir=C:\\zookeeper-3.4.10\\data
dataLogDir=C:\\zookeeper-3.4.10\\log
2.zk启动：到bin目录下，cmd输入zkServer.cmd
3.客户端连接zk：到bin目录下，cmd输入zkCli.cmd -server 127.0.0.1:2181
4.创建一个registry节点：接3操作，输入：create /registry data 即可

第一步：打包rpcdemo-serialize项目，生成jar包：mvn install -Dmaven.test.skip=true 
第二步：rpcdemo-server项目引入rpcdemo-serialize jar包，并打包jar包：mvn install -Dmaven.test.skip=true 
第三步：rpcdemo-service项目引入rpcdemo-server jar包: mvn install -Dmaven.test.skip=true

第四步：rpcdemo-client项目引入rpcdemo-serialize jar包，并打包jar包：mvn install -Dmaven.test.skip=true
第五步：rpcdemo-webtest项目引入rpcdemo-client 和 rpcdemo-service jar包: mvn install -Dmaven.test.skip=true

第六步：启动 rpcdemo-service项目
第七步：启动 rpcdemo-webtest项目

浏览器中输入：127.0.0.1:8080/index，即可看到效果。

rpcdemo-serialize：reqeust和response的定义，提供序列化工具
rpcdemo-server：服务地址注册到zk，服务启动（扫描特定的注解，代理实现方法调用）
rpcdemo-service：具体的服务

rpcdemo-client：从zk获取服务地址，服务请求和响应处理，rpc代理
rpcdemo-webtest：实际使用到rpc框架调用